# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.db.models import Q
from events_task.models import *
import datetime
import json
import ast
from django.db.models import Q
from .events import EventManager
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

def list(request):
    event_obj = Events.objects.filter(author=request.user.id).values()
    if event_obj:
        event_flag = None
        event_result_dict = {}
        for event in event_obj:
            if event_flag != event['type']:
                event_flag = event['type']
                event_ids = []
                event_filter = event_obj.filter(type=event_flag)
                for eve_obj in event_filter:
                    event_ids.append(eve_obj['event_mark'])
                    event_result_dict[eve_obj['type']] = {'count': event_filter.count(), 'ids': event_ids}
        return render(request, 'events.html', event_result_dict)
    else:
        return HttpResponse({'ERROR': 'Events are empty'}, status=204)


def delete(self, request, id):
    try:
        event = Events.objects.get(Q(id=id), (Q(author=request.user.id)| Q(event_participants__icontains=request.user)))
    except Events.DoesNotExist as e:
        return HttpResponse({'ERROR': str(e)}, status=400)
    event.delete()
    return HttpResponseRedirect('/home/')