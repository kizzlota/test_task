from django.apps import AppConfig


class EventsTaskConfig(AppConfig):
    name = 'events_task'
