# -*- coding: utf-8 -*-
from .models import Events

from channels import Group
import datetime
import json
from django.conf import settings
from django.contrib.auth import get_user_model

class EventManager:
    def __init__(self, type=None, action=None, read_at=None, event_mark=None, author=None, recepient=None, url=None, string=None):
        self.type = type
        self.action = action
        self.date_event = datetime.datetime.now()
        self.read_at = read_at
        self.event_mark = event_mark
        self.author = author
        self.recepient = recepient
        self.url = url
        self.string = string

    def event_create(self):
        # users = []
        # User = get_user_model()
        # for user in self.recepient:
            # user_recipient = User.objects.get(email=id)
            # users.append(user)
        event_instance = Events.objects.create(type=self.type, action=self.action, date_event=self.date_event,
                                               read_at=self.read_at, event_mark=self.event_mark, author=self.author,
                                               event_participants=[{'recepients':  self.recepient}], url=self.url,
                                               string=self.string
                                               )
        event_instance.save()
        if self.author:
            try:
                Group("event-%s" % self.author.id).send({
                    "text": json.dumps({'event': str(event_instance)}),
                })
            except:
                pass

        if self.recepient:
            for user in self.recepient:
                Group("event-%s" % user.id).send({
                    "text": json.dumps({'event': {'type': event_instance.type,
                                                  'id': event_instance.id,
                                                  'action': event_instance.action,
                                                  'date_event': event_instance.date_event.isoformat(),
                                                  'read_at': event_instance.read_at,
                                                  'event_mark': event_instance.event_mark,
                                                  'author': event_instance.author.id,
                                                  'url': event_instance.url,
                                                  'string': event_instance.string,
                                                  }}),
                })

    def event_update(self, type=None, action=None, date=None, read_at=None, event_mark=None, author=None, url=None, string=None):
        event_instance = Events.objects.get(type=self.type, event_mark=self.event_mark, author=self.author)
        event_instance.type = type
        event_instance.action = action
        event_instance.date = date
        event_instance.read_at = read_at
        event_instance.event_mark = event_mark
        event_instance.author = author
        event_instance.url = url
        event_instance.string = string
        event_instance.save()

        Group("event-%s" % event_instance.author.id).send({
            "text": json.dumps({'event': str(event_instance)}),
        })
        if self.recepient:
            for user in self.recepient:
                Group("event-%s" % user.id).send({
                    "text": json.dumps({'event': str(event_instance)}),
                })

    def event_delete(self, type=None, action='delete', event_mark=None, author=None):
        if not type or event_mark or author:
            event_instance = Events.objects.get(type=type, event_mark=event_mark, author=author)
            event_instance.delete()
            Group("event-%s" % event_instance.author.id).send({
                "text": json.dumps({'event': str(event_instance)}),
            })
            for user in self.recepient:
                Group("event-%s" % user.id).send({
                    "text": json.dumps({'event': str(event_instance)}),
                })
            # Group("event-%s" % self.recepient).send({
            #     "text": json.dumps({'event': str(event_instance)}),
            # })
