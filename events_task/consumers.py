from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import http_session_user, channel_session_user, channel_session_user_from_http
from django.db.models import Q
from django.conf import settings
from django.utils import timezone
import ast, sys
import datetime
import json
from profiles.models import User



@channel_session_user_from_http
@channel_session
def event_connect(message):
    # Add them to the right group
    # serializer = MessageSerializer(data=request.data)
    Group("event-%s" % message.user.id).add(message.reply_channel)



@channel_session_user_from_http
@channel_session_user
def event_disconnect(message):
    if not message.user.is_anonymous():
        message.user.last_activity = timezone.now()
        message.user.save()
    Group("event-%s" % message.user.id).discard(message.reply_channel)
