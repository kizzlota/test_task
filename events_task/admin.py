from django.contrib import admin
from events_task.models import Events
# Register your models here.

@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'type', 'action', 'date_event', 'read_at', 'event_mark', 'author'
    ]