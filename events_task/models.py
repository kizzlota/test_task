# coding=utf-8
from django.conf import settings
import os
from django.db.models import signals
from django.utils import timezone
from django.db import models
from channels import Channel, Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.postgres.fields import JSONField
import json
from celery.task import task


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class Events(models.Model):
    """
    таблиця повідомлень, сповіщень
    """
    type = models.CharField(max_length=200, blank=True, null=True)
    action = models.CharField(max_length=200, blank=True, null=True)
    date_event = models.DateTimeField(auto_now_add=True)
    read_at = models.DateTimeField(blank=True, null=True)
    event_mark = models.IntegerField(blank=True, null=True)  # ID of type event example: Job --> id=45
    author = models.ForeignKey(AUTH_USER_MODEL, blank=True, null=True, related_name='event_author',
                               verbose_name="Author")
    event_participants = models.CharField(max_length=300, blank=True, null=True)
    url = models.CharField(max_length=300, blank=True, null=True)
    string = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"