from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'email', 'first_name', 'second_name', 'last_name', 'is_staff', 'is_active',
                    'date_joined'
                    ]
    search_fields = ['id', 'username', 'email', 'first_name']
    list_filter = ['id', 'username', 'email']