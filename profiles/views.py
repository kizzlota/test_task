from django.shortcuts import render
from django.http import HttpResponseRedirect
# Create your views here.


def base_page(request):
        if request.user.is_authenticated:
            context = {
                'user':request.user,
            }
        else:
            context = {
                'user': 'anonymous'
            }
        return render(request, 'home.html', context=context)