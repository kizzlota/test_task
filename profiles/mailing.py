from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail.message import EmailMultiAlternatives
from binascii import hexlify
from os import urandom
# from .models import RegistrationCode
import datetime
from django.core.mail.message import EmailMultiAlternatives
from celery import shared_task
from celery.task import task


@task(default_retry_delay=3 * 20, max_retries=3)
def send_email(parameters):
    print(parameters.get('source'), parameters.get('prefix'))
    subject_file = 'mail/{0}/{1}_subject.txt'.format(parameters.get('source'), parameters.get('prefix'))
    txt_file = 'mail/{0}/{1}.txt'.format(parameters.get('source'), parameters.get('prefix'))
    html_file = 'mail/{0}/{1}.html'.format(parameters.get('source'), parameters.get('prefix'))
    subject = render_to_string(subject_file).strip()
    from_email = settings.DEFAULT_EMAIL_FROM
    to = parameters.get('user_email')
    ctxt = {
        'email': parameters.get('user_email'),
        'username': parameters.get('username'),
        'link': parameters.get('link')
    }
    text_content = render_to_string(txt_file, ctxt)
    html_content = render_to_string(html_file, ctxt)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, 'text/html')

    msg.send()

#