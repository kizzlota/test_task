from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils import timezone
import os
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime
from django.template.loader import render_to_string
from django.core.mail.message import EmailMultiAlternatives

# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, username, email=None, password=None, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(username=username, email=self.normalize_email(email),
                          last_login=now, date_joined=now, **extra_fields)
        # user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        user = self.create_user(username=username, email=email, password=password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=50, unique=True,
                                help_text=
                                    'Required. 30 characters or fewer. Letters, numbers and @/./+/-/_ characters')
    email = models.EmailField(max_length=255, unique=True, null=True, verbose_name='e-mail')
    first_name = models.CharField('first name', max_length=30, blank=True, null=True)
    last_name = models.CharField('last name', max_length=30, blank=True, null=True)
    second_name = models.CharField('second name', max_length=30, blank=True, null=True)

    is_staff = models.BooleanField(default=False,
                                   help_text='Designates whether the user can log into this admin site.')
    is_active = models.BooleanField(default=True)


    date_joined = models.DateTimeField('date joined', default=timezone.now)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    # def email_user(self, subject, message, from_email=None):
    #    send_mail(subject, message, from_email, [self.email])

    def __str__(self):
        return str(self.username)  # or u''

