from events_task import consumers
from channels.routing import route, include



event_routing = [
    route('websocket.connect', consumers.event_connect),
    # route('websocket.receive', consumers.event_message),
    route('websocket.disconnect', consumers.event_disconnect),
]


channel_routing = [
    # You can use a string import path as the first argument as well.
    include(event_routing, path=r"^/event/$"),
    #  include(http_routing),
]
