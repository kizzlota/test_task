from django import forms
from .models import Task, UserFiles, STATUS_CHOICES, TASK_STATUS
from crispy_forms.helper import FormHelper

class TaskForm(forms.ModelForm):
    title = forms.CharField(label='task_title',required=True)
    description = forms.CharField(label='description of task', widget=forms.TextInput)
    status = forms.CharField(max_length=20, widget=forms.Select(choices=STATUS_CHOICES))
    privacy = forms.CharField(max_length=20, widget=forms.Select(choices=TASK_STATUS))

    class Meta:
        model = Task
        fields = ['title','description', 'status', 'privacy']

    def clean_title(self):
        title = self.cleaned_data.get('title')
        check_task_title = Task.objects.filter(title=title)
        if check_task_title.exists():
            raise forms.ValidationError("email is already exists")
        else:
            return title

    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['placeholder'] = u'title*'
        self.fields['title'].label = ''
        self.fields['description'].widget.attrs['placeholder'] = u'description'
        self.fields['description'].label = ''
        self.fields['status'].widget.attrs['placeholder'] = u'status'
        self.fields['status'].label = 'choose your status'
        self.fields['privacy'].widget.attrs['placeholder'] = u'privacy'
        self.fields['privacy'].label = 'privacy of task'