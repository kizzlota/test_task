from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

# from tasks.views import TaskView
from tasks.views import *


urlpatterns = [
    #url(r'', TaskView.as_view(), name='task'),
    url(r'^public_tasks/$', public_tasks, name='public'),
    url(r'^public_my_tasks/$', public_my_tasks, name='public_my'),
    url(r'^private_tasks/$', private_tasks, name='private_tasks'),
    url(r'^update_task/(?P<id>[0-9]+)/$', update_task, name='update_task'),
    url(r'^delete_task/(?P<id>[0-9]+)/$', delete_task, name='delete_task'),
    url(r'', task_all, name='home'),
    # url(r'', TaskView.as_view(), name='home'),

]