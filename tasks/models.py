from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
import os
from django.conf import settings
import uuid


# Create your models here.

STATUS_CHOICES = (
    ('1', 'open'),
    ('2', 'processing'),
    ('3', 'closed'),
    )
TASK_STATUS = (
    ('1', 'public'),
    ('2', 'private'),
)



def get_file_path(self, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(self.folder_name, self.user_name, filename)


class UserFiles(models.Model):
    file = models.FileField(blank=True, null=True, upload_to=get_file_path)
    date_of_add = models.DateField(auto_now_add=True, blank=True, null=True)
    name_file = models.CharField(blank=True, null=True, max_length=100)
    relation_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='related_user',
                                         verbose_name='relation to user')

    def __str__(self):
        return str(self.name_file)  # or u''

    @property
    def user_name(self):
        user_name = self.relation_to_user.username
        return user_name

    @property
    def folder_name(self):
        folder = 'user_files'
        return folder


class Task(models.Model):
    title = models.CharField(max_length=120, verbose_name='title task')
    status = models.CharField(max_length=20, blank=True, null=True, default='open', choices=STATUS_CHOICES)
    privacy = models.CharField(max_length=20, blank=True, null=True, default='public', choices=TASK_STATUS)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    read = models.BooleanField(default=False)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='updated_by',
                               verbose_name='task author')
    description = models.TextField(max_length=500, blank=True, null=True)
    relation_to_files = models.ManyToManyField(UserFiles, blank=True)

    def relation_to_user_files(self):
        return self.relation_to_files.all()

    def __str__(self):
        return str(self.title)  # or u''
