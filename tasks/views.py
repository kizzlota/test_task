from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect, HttpResponse
from .forms import TaskForm
from .models import Task
from django.contrib import messages
from django.core.urlresolvers import reverse
from events_task.events import EventManager
from profiles.mailing import send_email
# Create your views here.


def public_tasks(request, *args, **kwargs):
    if request.user.is_authenticated:
        tasks_objects = Task.objects.all()
        return render(request, 'public_tasks.html', {'task': tasks_objects})
    return HttpResponse({"error": "you need to be authen..."}, status=400)


def public_my_tasks(request, *args, **kwargs):
    if request.user.is_authenticated:
        tasks_objects = Task.objects.filter(privacy='1', author=request.user)
        if tasks_objects:
            return render(request, 'public_tasks.html', {'task': tasks_objects})
        else:
            return HttpResponse({"error": "no data to show..."}, status=400)
    return HttpResponse({"error": "you need to be authen..."}, status=400)


def private_tasks(request, *args, **kwargs):
    tasks_objects = Task.objects.filter(privacy='2', author=request.user)
    context = {
        'form': TaskForm,
        'task': tasks_objects,
        }
    if request.method == "POST":
        form = TaskForm(request.POST or None)
        if form.is_valid():
            form.save(commit=False)
            messages.success(request, 'OK')
            event = EventManager(type='task',
                                 action='update',
                                 event_mark=form.cleaned_data.get('id'),
                                 author=request.user,
                                 recepient=[request.user],  # recipient is need then anybody must events
                                 url='',
                                 string='new task created %s' % form.cleaned_data.get('title')
                                 )
            event.event_create()
            send_email.delay({'username': str(request.user.username),
                              'user_email': str(request.user.email),
                              'prefix': 'task_update',
                              'source': 'task_mail',
                              'sender': str(request.user.username)})
            messages.success(request, 'congrats! your data is saved to db')

            return HttpResponseRedirect('/private_tasks/')
    if tasks_objects:
        return render(request, 'private_tasks.html', context)
    else:
        return HttpResponse({"error": "no data to show..."}, status=400)


def update_task(request, id):
    try:
        task_instance = Task.objects.get(author=request.user, id=id)
    except Task.DoesNotExist as error:
        return HttpResponse({'error': str(error)}, status=400)
    if request.method == 'POST':
        form = TaskForm(request.POST or None, instance=task_instance)
        if form.is_valid():
            form_task = form.save(commit=False)
            form.author = request.user
            form_task.read = False
            event = EventManager(type='task',
                                 action='update',
                                 event_mark=form.cleaned_data.get('id'),
                                 author=request.user,
                                 recepient=[request.user],  # recipient is need then anybody must events
                                 url='',
                                 string='new task created %s' % form.cleaned_data.get('title')
                                 )
            event.event_create()
            send_email.delay({'username': str(request.user.username),
                              'user_email': str(request.user.email),
                              'prefix': 'task_create',
                              'source': 'task_mail',
                              'sender': str(request.user.username)})
            form_task.save()
            messages.success(request, 'task {task_titile} updated successfully'.format(task_titile=form_task.id))


def delete_task(request, id):
    try:
        task_instance = Task.objects.get(author=request.user, id=id)
    except Task.DoesNotExist as error:
        return HttpResponse({'error': str(error)}, status=400)
    task_instance.delete()
    event = EventManager(type='task',
                         action='update',
                         event_mark=task_instance.id,
                         author=request.user,
                         recepient=[request.user],  # recipient is need then anybody must events
                         url='',
                         string='new task created %s' % task_instance.title
                         )
    event.event_create()
    messages.warning(request, 'deleted successfully')
    return HttpResponseRedirect('/private_tasks/')


def task_all(request):
    if request.user.is_authenticated:
        tasks_objects = Task.objects.all()
    else:
        tasks_objects = Task.objects.filter(privacy='1')
    if request.method == 'GET':
        context = {
            'task': tasks_objects,
            'form': TaskForm,
        }
        return render(request, 'tasks.html', context=context)
    if request.method == "POST":
        if request.user.is_authenticated:
            form = TaskForm(request.POST or None)
            if form.is_valid():
                sav_form = form.save(commit=False)
                sav_form.author = request.user
                sav_form.save()
                event = EventManager(type='task',
                                     action='create',
                                     event_mark=form.cleaned_data.get('id'),
                                     author= request.user,
                                     recepient=[request.user],  #recipient is need then anybody must events
                                     url='',
                                     string='new task created %s' % form.cleaned_data.get('title')
                                    )
                event.event_create()
                send_email.delay({'username': str(request.user.username),
                                                      'user_email': str(request.user.email),
                                                      'prefix': 'task_create',
                                                      'source': 'task_mail',
                                                      'sender': str(request.user.username)})
                print('done')
                # except OSError:
                #     return HttpResponse('shit happens, check your mail backend settings')
                messages.success(request, 'congrats! your data is saved to db')
                # add mailing
                return HttpResponseRedirect('/task/')
        return HttpResponseRedirect('/home/')

# class TaskView(View):
#     form = TaskForm
#
#     def get(self, request, *args, **kwargs):
#         print('METHOD GET')
#         # rensder all users tasks
#         if request.user.is_authenticated:
#             user_task = Task.objects.all()
#         context = {
#             'task': user_task,
#             'form': self.form
#
#         }
#         return render(request, 'private_tasks.html', context=context)
#
#     def post(self, request, *args, **kwargs):
#         print('METHOD POST')
#         form = self.form(request.POST or None)
#         if form.is_valid():
#             sav_form = form.save(commit=False)
#             sav_form.author = request.user
#             sav_form.save()
#             message = messages.success(request, 'congrats! your data is saved to db')
#             return HttpResponseRedirect('/task/')
#         else:
#             return render(request, 'home.html', {'from': self.form})
