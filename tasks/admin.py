from django.contrib import admin
from .models import *
# Register your models here.


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'privacy', 'author', 'description',
                    ]
    search_fields = ['id', 'title', 'author']
    list_filter = ['id', 'title']
    list_editable = ['title', 'privacy']

@admin.register(UserFiles)
class UserFilesAdmin(admin.ModelAdmin):
    list_display = ['id', 'file', 'date_of_add', 'name_file', 'relation_to_user'
                    ]
    search_fields = ['id', 'name_file', 'relation_to_user']
    list_filter = ['id', 'name_file']